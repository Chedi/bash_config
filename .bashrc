# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
        RED="\[\033[0;31m\]"
     YELLOW="\[\033[1;33m\]"
      GREEN="\[\033[0;32m\]"
       BLUE="\[\033[1;34m\]"
  LIGHT_RED="\[\033[1;31m\]"
LIGHT_GREEN="\[\033[1;32m\]"
      WHITE="\[\033[1;37m\]"
 LIGHT_GRAY="\[\033[0;37m\]"
 COLOR_NONE="\[\e[0m\]"


# Reset
Color_Off='\e[0m'       # Text Reset

# Regular Colors
 Black='\e[0;30m'       # Black
   Red='\e[0;31m'       # Red
 Green='\e[0;32m'       # Green
Yellow='\e[0;33m'       # Yellow
  Blue='\e[0;34m'       # Blue
Purple='\e[0;35m'       # Purple
  Cyan='\e[0;36m'       # Cyan
 White='\e[0;37m'       # White

# Bold
 BBlack='\e[1;30m'      # Black
   BRed='\e[1;31m'      # Red
 BGreen='\e[1;32m'      # Green
BYellow='\e[1;33m'      # Yellow
  BBlue='\e[1;34m'      # Blue
BPurple='\e[1;35m'      # Purple
  BCyan='\e[1;36m'      # Cyan
 BWhite='\e[1;37m'      # White

# Underline
 UBlack='\e[4;30m'      # Black
   URed='\e[4;31m'      # Red
 UGreen='\e[4;32m'      # Green
UYellow='\e[4;33m'      # Yellow
  UBlue='\e[4;34m'      # Blue
UPurple='\e[4;35m'      # Purple
  UCyan='\e[4;36m'      # Cyan
 UWhite='\e[4;37m'      # White

# Background
 On_Black='\e[40m'      # Black
   On_Red='\e[41m'      # Red
 On_Green='\e[42m'      # Green
On_Yellow='\e[43m'      # Yellow
  On_Blue='\e[44m'      # Blue
On_Purple='\e[45m'      # Purple
  On_Cyan='\e[46m'      # Cyan
 On_White='\e[47m'      # White

# High Intensity
 IBlack='\e[0;90m'      # Black
   IRed='\e[0;91m'      # Red
 IGreen='\e[0;92m'      # Green
IYellow='\e[0;93m'      # Yellow
  IBlue='\e[0;94m'      # Blue
IPurple='\e[0;95m'      # Purple
  ICyan='\e[0;96m'      # Cyan
 IWhite='\e[0;97m'      # White

# Bold High Intensity
 BIBlack='\e[1;90m'     # Black
   BIRed='\e[1;91m'     # Red
 BIGreen='\e[1;92m'     # Green
BIYellow='\e[1;93m'     # Yellow
  BIBlue='\e[1;94m'     # Blue
BIPurple='\e[1;95m'     # Purple
  BICyan='\e[1;96m'     # Cyan
 BIWhite='\e[1;97m'     # White

# High Intensity backgrounds
 On_IBlack='\e[0;100m'  # Black
   On_IRed='\e[0;101m'  # Red
 On_IGreen='\e[0;102m'  # Green
On_IYellow='\e[0;103m'  # Yellow
  On_IBlue='\e[0;104m'  # Blue
On_IPurple='\e[0;105m'  # Purple
  On_ICyan='\e[0;106m'  # Cyan
 On_IWhite='\e[0;107m'  # White

# Detect whether the current directory is a git repository.
function is_git_repository {
    git branch > /dev/null 2>&1
}

# Determine the branch/state information for this git repository.
function set_git_branch {
# Capture the output of the "git status" command.
    git_status="$(git status 2> /dev/null)"

# Set color based on clean/staged/dirty.
    if [[ ${git_status} =~ "working directory clean" ]]; then
        state="${GREEN}"
    elif [[ ${git_status} =~ "Changes to be committed" ]]; then
        state="${YELLOW}"
    else
        state="${LIGHT_RED}"
    fi

# Set arrow icon based on status against remote.
    remote_pattern="# Your branch is (.*) of"
    if [[ ${git_status} =~ ${remote_pattern} ]]; then
        if [[ ${BASH_REMATCH[1]} == "ahead" ]]; then
            remote="↑"
        else
            remote="↓"
        fi
    else
        remote=""
    fi
    diverge_pattern="# Your branch and (.*) have diverged"
    if [[ ${git_status} =~ ${diverge_pattern} ]]; then
        remote="↕"
    fi

# Get the name of the branch.
    branch_pattern="^# On branch ([^${IFS}]*)"
    if [[ ${git_status} =~ ${branch_pattern} ]]; then
        branch=${BASH_REMATCH[1]}
    fi

# Set the final branch string.
    BRANCH="\342\224\200${state}\[\033[1;37m\]<<${BIYellow}${branch}\[\033[1;37m\]>>${remote}${COLOR_NONE}"
}

# Return the prompt symbol to use, colorized based on the return value of the
# previous command.
function set_prompt_symbol () {
    if test $1 -eq 0 ; then
        PROMPT_SYMBOL="\$"
    else
        PROMPT_SYMBOL="${LIGHT_RED}\$${COLOR_NONE}"
    fi
}

# Determine active Python virtualenv details.
function set_virtualenv () {
    if test -z "$VIRTUAL_ENV" ; then
        PYTHON_VIRTUALENV=""
    else
        PYTHON_VIRTUALENV="\342\224\200(${RED}`basename \"$VIRTUAL_ENV\"`${COLOR_NONE}\[\033[1;37m\])"
    fi
}

# Set the full bash prompt.
function linux_bash_prompt () {
# Set the PROMPT_SYMBOL variable. We do this first so we don't lose the
# return value of the last command.
    set_prompt_symbol $?

# Set the PYTHON_VIRTUALENV variable.
    set_virtualenv

# Set the BRANCH variable.
    if is_git_repository ; then
        set_git_branch
    else
        BRANCH=''
    fi

    LINE="\342\224\200"
    ARROW="${LINE}>"
    CORNER_UP="\342\224\214"
    CORNER_DOWN="\342\224\224"
    DATE_PROMPT="${WHITE}(\[\033[1;34m\]\@ \d${WHITE})"
    FILES_NUMBER_PROMPT=`ls -1 | wc -l | sed 's: ::g'`
    FILES_SIZE_PROMPT=`ls -lah | grep -m 1 total | sed 's/total //'`
    DISK_USAGE_INFO=`df -h "$PWD" | awk '/[0-9]/{print $(NF-2)}'`
    INODE_USAGE_INFO=`df -i "$PWD" | awk '/[0-9]/{print $(NF-2)}'`
    DISK_USAGE_PERCENTAGE=`df -h "$PWD" | awk '/[0-9]/{print $(NF-1)}' | sed 's/%//'`

    if [[ $DISK_USAGE_PERCENTAGE -ge 80 ]]; then
        DISK_USAGE_INDICATOR="${WHITE}(${BIRed}☢ ${INODE_USAGE_INFO} ${DISK_USAGE_INFO} ${DISK_USAGE_PERCENTAGE}%${WHITE})"
    elif [[ $DISK_USAGE_PERCENTAGE -ge 50 ]]; then
        DISK_USAGE_INDICATOR="${WHITE}(${BIYellow}☯ ${INODE_USAGE_INFO} ${DISK_USAGE_INFO} ${DISK_USAGE_PERCENTAGE}%${WHITE})"
    else
        DISK_USAGE_INDICATOR="${WHITE}(${BIGreen}★ ${INODE_USAGE_INFO} ${DISK_USAGE_INFO} ${DISK_USAGE_PERCENTAGE}%${WHITE})"
    fi


    if [[ ${EUID} == 0 ]]; then
        USER_PROMPT="${WHITE}${CORNER_UP}(${LIGHT_RED}\h${WHITE})"
    else
        USER_PROMPT="${WHITE}${CORNER_UP}(${BLUE}\u@\h${WHITE})"
    fi

    if [[ $? -ne 0 ]]; then
        LAST_RESPONSE_PROMPT=" ${RED}\342\234\227 "
    else
        LAST_RESPONSE_PROMPT=" ${RED}\342\234\224 "
    fi

    #BATTERY_LEVEL=`acpi -b | grep -Eo '[0-9]+%'`
    #REMAINING_TIME=`acpi -b | grep -Eo '[0-9]{2}:[0-9]{2}:[0-9]{2}'`
    #BATTERY_LEVEL_VALUE=${BATTERY_LEVEL:0:-1}
    #BATTERY_STATUS_IDX=`expr $BATTERY_LEVEL_VALUE / 10`

    #if [[ $BATTERY_LEVEL_VALUE -le 30 ]]; then
    #    BATTERY_COLOR="${RED}"
    #elif [[ $BATTERY_LEVEL_VALUE -le 60 ]]; then
    #    BATTERY_COLOR="${YELLOW}"
    #else
    #    BATTERY_COLOR="${GREEN}"
    #fi

    #BATTERY_INDICATOR="${WHITE}${LINE}(${BATTERY_COLOR}${REMAINING_TIME} ${BATTERY_LEVEL}${WHITE})"

#${USER_PROMPT}${LINE}${DISK_USAGE_INDICATOR}${BATTERY_INDICATOR}${WHITE}${LINE}$DATE_PROMPT${PYTHON_VIRTUALENV}${BRANCH}
    PS1="
${USER_PROMPT}${LINE}${DISK_USAGE_INDICATOR}${WHITE}${LINE}$DATE_PROMPT${PYTHON_VIRTUALENV}${BRANCH}
${WHITE}${CORNER_DOWN}${LINE}(${GREEN}\w${WHITE})${LINE}(${GREEN}${FILES_NUMBER_PROMPT} files, ${FILES_SIZE_PROMPT}b${WHITE})${ARROW} ${COLOR_NONE}"
}

function power_shell()
{
    export PS1="$(~/.powerline-shell.py $? 2> /dev/null)"
}

git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%C(bold blue)<%an>%Creset' --abbrev-commit"

alias pong="ping 8.8.8.8 -c 4"
alias kappa='setxkbmap -option ctrl:nocaps'
alias pylint='pylint --rcfile=~/.pylint.rc'

lv        (){ livestreamer -p mpv "$1" $2;}
lvt       (){ livestreamer -p mpv "twitch/tv$1" $2;}

lvlow     (){ livestreamer -p mpv "$1" low   ; }
lvbest    (){ livestreamer -p mpv "$1" best  ; }
lvaudio   (){ livestreamer -p mpv "$1" audio  ; }
lvmedium  (){ livestreamer -p mpv "$1" medium; }

lvtlow    (){ livestreamer -p mpv "twitch.tv/$1" low   ; }
lvtbest   (){ livestreamer -p mpv "twitch.tv/$1" best  ; }
lvtaudio  (){ livestreamer -p mpv "twitch.tv/$1" audio ; }
lvtmedium (){ livestreamer -p mpv "twitch.tv/$1" medium; }

mosh_gcloud(){
    instance_ip=`gcloud compute instances list | grep $1 | awk '{print $5}'`;
    echo $instance_ip;
    mosh --ssh="ssh -i ~/.ssh/google_compute_engine -o UserKnownHostsFile=/dev/null -o CheckHostIP=no -o StrictHostKeyChecking=no" $instance_ip;
}

# The next line updates PATH for the Google Cloud SDK.
export PATH=/home/chedi/Devel/Tools/google-cloud-sdk/bin:$PATH:/home/chedi/Tools/bin:/home/chedi/cabal/bin

# Updating all the packages in virtualenv
alias pip_update_all='pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U'

export EDITOR="emacsclient --alternate-editor="" --no-wait -n $*"
export WORKON_HOME="/var/cache/virtualenv/"
export PROMPT_COMMAND=linux_bash_prompt
export ALTERNATE_EDITOR=""

if [ ! -d "$WORKON_HOME" ]; then
    sudo mkdir $WORKON_HOME
    sudo chown chedi $WORKON_HOME
fi

if [ ! -f "$WORKON_HOME/postmkvirtualenv" ]; then
    echo "pip install jedi flake8 importmagic virtualenvwrapper" > $WORKON_HOME/postmkvirtualenv
fi

source `which virtualenvwrapper.sh`
